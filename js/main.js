function toggleMenu(){
    let toggler = document.querySelector('.toggle');
    let navigationToggler = document.querySelector('.navigation');
    let mainToggler = document.querySelector('#main');
    toggler.classList.toggle('active');
    navigationToggler.classList.toggle('active');
    mainToggler.classList.toggle('active');
}